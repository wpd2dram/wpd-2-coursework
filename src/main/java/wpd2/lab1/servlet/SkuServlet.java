package wpd2.lab1.servlet;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class SkuServlet extends BaseServlet {

    private static final String SKU_TEMPLATE = "shop.mustache";

    public SkuServlet() {
    }

    private Object getObject() {
        return new Sku()
                .setTitle("skuId")
                .setPrice("10.95")
                .setDescription("Short Description")
                .setImage(new SkuImage().setName("bouncycastle.jpg"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        showView(response, SKU_TEMPLATE, getObject());
    }

    @Data
    @Accessors(chain = true)
    private static class SkuImage {
        private String name;
    }

    @Data
    @Accessors(chain = true)
    private static class Sku {
        private String title;
        private String price;
        private String description;
        private SkuImage image;
    }

}

