var template, data, html;
var gotTemplate, gotData;


function getProducts() {

    gotTemplate = gotData = false;

    $.get("../Mustache/productListTemplate.mustache", null, function (ajaxData) {
        template = ajaxData;
        gotTemplate = true;
        if (gotData) processTemplate();
    });

    $.getJSON("../JSON/products.json", null, function (ajaxData) {
        data = ajaxData;
        gotData = true;
        if (gotTemplate) processTemplate();
    });
}

function processTemplate() {
    html = Mustache.render(template, data);
    $('#productList').html(html);

}

