var template1, data1, html;
var gotTemplate1, gotData1;

function getWishList() {

    gotTemplate1 = gotData1 = false;

    $.get("../Mustache/wishListTemplate.mustache", null, function (ajaxData1) {
        template1 = ajaxData1;
        gotTemplate1 = true;
        if (gotData1) processTemplate1();
    });

    $.getJSON("../JSON/wishList.json", null, function (ajaxData1) {
        data1 = ajaxData1;
        gotData1 = true;
        if (gotTemplate1) processTemplate1();
    });
}

function processTemplate1() {

    html = Mustache.render(template1, data1);
    $('#wishList').html(html);
}

function deleteRow(btn){
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}