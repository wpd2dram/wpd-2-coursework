#README - Web Platform Development 2: Coursework#

#Group Report can be found in Folder 02-Coursework and a file named WPD2-Group-Report-Final.doc#

##Running the Application##

1. Run the Runner
2. Open a Web Browser
3. go to: localhost:9000
* This will take you to the main index.html page
4. Click on Login/Logout
* This will take you to a Login Page (or you could got to: localhost:9000/login)
5. Type in a user name and password
6. Click register
* You are now logged in and should be at index.html
7. Click on My Messages
* This will take you to the messenger page
8. Test a Message
9. Click on: Back to Congo.co.uk
* You should now be at the index.html page

##Purpose of Repository##

This Repository is for the submission of coursework for Web Platform Development

##Contribution guidelines##

* The Master Branch has four sub-branches:
	* amoore204 - Alan Thomas Moore (or Joalan Moorelaney)
	* djones201 - David jones (or djones201)
	* rhendr203 - Ross Hendry (or rossh29)
	* mconwa201 - Mark Conway
* Each Group Member will write their code in their contribution branch
* The Master Branch is where all contributions will be merged
* Any Merge with the Master Branch needs to be agreed by the group

#Project Contacts#

##Module Leader##
###Katherine Hartmann###
*k.hartmann@gcu.ac.uk*

###Alan Thomas Moore###
*amoore204@caledonian.ac.uk*
**S1436102**

###David Jones###
*djones201@caledonian.ac.uk*
**S1433087**

###Ross Hendry###
*rhendr203@caledonian.ac.uk*
**S1227760**

###Mark Conway###
*mconwa201@caledonian.ac.uk*
**S1424239**